import os
import glob
from setuptools import Extension, setup, Distribution

# def read(fname):
#     return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "pymkews_client",
    description="This is the python package to mimic the web-interface functionalities using Python API",
    # version = "@PYMKEWS_CLIENT_VERSION@",
    author = "Magik Eye Inc.",
    author_email = "jigar@magik-eye.com",
    license = "Proprietary, DO NOT DISTRIBUTE",
    keywords = "mkews pymkews client",
    url = "https://bitbucket.org/mke_rnd/mkews",
    packages=[
        'pymkews_client',
    ],
    entry_points ={ 
            'console_scripts': [ 
                'pymkews_client_cli = pymkews_client.pymkews_client_cli:main'
            ] 
    }, 
    # package_data={
    #   'pymkews_controller.job_executor': ['exec.slurm']
    # },
    python_requires=">=3.6",
    install_requires=[
        'asgiref==3.2.10',
        'certifi==2020.6.20',
        'chardet==3.0.4',
        'Django==3.1.1',
        'idna==2.10',
        'pytz==2020.1',
        'requests==2.24.0',
        'sqlparse==0.3.1',
        'urllib3==1.25.10'
    ],
    # long_description=read('@CMAKE_CURRENT_SOURCE_DIR@/README.md'),
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Topic :: Scientific/Engineering ",
        "License :: Other/Proprietary License",
    ]
)