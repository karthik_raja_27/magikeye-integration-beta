#!/usr/bin/env bash

# Author : Jigar Patel
# Copyright (c) 2020, Magik-Eye Inc.

# Log level
# Set verbose mode ON
# pymkews_client_cli -v
# Set quiet mode ON
# pymkews_client_cli -u test_user_1 -q

# Datasets utility
# 1. List datasets 
# DTSS=`pymkews_client_cli -ld -v`
# echo "List of datasets"
# echo "$DTSS"

# 2. Create dataset
# DTSID=`pymkews_client_cli -cd -v`
# DTSID=`pymkews_client_cli --create_dataset --dataset_comment comment1`
# echo "Created dataset, $DTSID"

# 3. Update dataset - file/comment
# `pymkews_client_cli -uc comment2 -did $DTSID`
# `pymkews_client_cli -uf "data/magik_eye1.png" -did $DTSID -v`

# 4. Delete dataset
# `pymkews_client_cli -dd 120 -v`

# 5. List files in dataset
# FILES=`pymkews_client_cli -ldf 121`
# echo "List of files : "
# echo "$FILES"

# 6. get dataset files
# `pymkews_client_cli -v -gdf 'magik_eye3.png' -fsp "/home/jigar/Desktop" -did 121`

# Jobs utility
# 1. List jobs
# JOBS=`pymkews_client_cli -lj`
# echo "List of jobs"
# echo "$JOBS"

# 2. Create job
# JOBID=`pymkews_client_cli -cj "data/job1.json"`
# echo "Jobs created with job id : $JOBID"

# 3. Delete job
# `pymkews_client_cli -dj 149 -v`

# 4. Get job files
# `pymkews_client_cli -v -gjf -jid 147 -fsp '/home/jigar/Desktop'`

# 5. Query jobs
# STATUS=`pymkews_client_cli -qj 145 -v`
# echo "$STATUS"
