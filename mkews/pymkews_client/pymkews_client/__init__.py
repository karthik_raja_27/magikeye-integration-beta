# -*- coding: utf-8 -*-

"""
PYMkEWS client
"""

__author__ = "Jigar patel"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."
__version__ = '1.0.0'

from .client import Client