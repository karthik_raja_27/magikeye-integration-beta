# -*- coding: utf-8 -*-

"""
PYMKEWS client
"""

__author__ = "Jigar patel"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."
__version__ = '1.0.0'

# ------------------------------------------------------------------------------

import logging
import requests
import json
import os
from typing import List, IO
from django.http import HttpResponse
import mimetypes
import getpass

# ------------------------------------------------------------------------------

home_path = os.getenv("HOME")
file_name = "log_file"

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.ERROR)

fileHandler = logging.FileHandler("{0}/{1}.log".format(home_path+"/mkews", file_name))
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)

db_url = "http://127.0.0.1:8002/"
web_url = "http://127.0.0.1:8000/"

# ------------------------------------------------------------------------------

class Client(object):
    """Used to access and edit the database and jobs of Magik Eye Web Server.

    :param username: Username provided by Magik-eye
    :type iface: str, optional
    :param password: Password provided by Magik-eye
    :type iface: str, optional
    """
    
    def __init__(self,username=None,password=None):
        """Constructor method
        """
        with open(home_path+'/mkews/config.json') as f:
            data = json.load(f)
        
        self.username = data['username']
        self.password = data['password']
        if(username):
            self.username = username
        if(password):
            self.password = password
        data = {'username': self.username,
                'password' : self.password}
        data = json.dumps(data)
        
        dataset_url = "api/login/"
        url = web_url + dataset_url

        # API Call
        response = requests.post(url, data=data)
        if(response.status_code==200):
            logging.info('User successfully logged in')
            token = response.json()['secret_token']
            self.token = token
        else:
            logging.error('You have entered an invalid username or password!')

    def _tokenize_message(self,data):
        status = False
        try:
            data["secret_token"] = self.token
            status = True
        except:
            logging.error('Please Login first')
        return status

    def set_log_level(self,level:int):
        """Set level for logging :
        1. Set 0 for Verbose mode. (INFO) 
        2. Set 1 for Quiet mode. (ERROR)

        Args:
            level (int): 0 or 1
        """
        if(level==0):
            rootLogger.setLevel(logging.INFO)
            logging.info("Log level now set to INFO")
        elif(level==1):
            rootLogger.setLevel(logging.ERROR)
            
    # Datasets --------------------------------------------------------------------------------------------------------------------------------
        
    def create_dataset(self,comment=None,file_path=None)->str:
        """Creates a new dataset w/wo files w/wo comments.

        Args:
            comment (str, optional): Update comment for dataset.
            file_path (str, optional): Update filepath for dataset.

        Returns:
            int: dataset_id [-1 in case error while creating dataset]
        """
        
        dataset_url = "api/createDataSet/"
        url = db_url + dataset_url
        data = {'username': self.username}
        dataset_id = -1
        if(self._tokenize_message(data)):
            data = json.dumps(data)
            
            # API Call
            response1 = requests.post(url, data=data)
            dataset_id = int(response1.json()['data_set_id'])
            
            # FS createFSDataset API
            fs_dataset_url = 'api/createFSDataset/'
            url = db_url + fs_dataset_url
            data = {'username': self.username,
                    'data_set_id': dataset_id}
            self._tokenize_message(data)
            data = json.dumps(data)
            
            # FS API Call
            response2 = requests.post(url, data=data)
            
            # Update comment in dataset
            if(comment):
                self.update_comment(dataset_id,comment)
            
            # Update file in dataset
            if(file_path):
                self.update_file(dataset_id,file_path)

            message = f"Dataset successfully created with id : "+str(dataset_id)+"."
            
            if(response1.status_code == 200 and response2.status_code == 200):
                logging.info(message)
            else:
                logging.error("Error creating dataset")
            
        return dataset_id
    
    # -----------------------------------------------------------------------------------------------------------------------------------------

    def update_file(self,dataset_id:int,file_path:str)->bool:
        """ Adds files to exisiting dataset.

        Args:
            dataset_id (int): 
            file_path (str): 

        Returns:
            bool: status describing if the call was success or failed.
        """
        
        fs_updatefile_url = 'api/addFileToFSDataset/'
        url = db_url + fs_updatefile_url
        data = { 'username'    : self.username,
                 'data_set_id' : dataset_id,
                 'filename'    : file_path.split('/')[-1] }
        files = {'file' : open(file_path,"rb")}
        
        status = False
        if(self._tokenize_message(data)):
            response = requests.post(url, data=data,files = files)
            
            message = "File successfully updated for dataset id: "+str(dataset_id)+"."

            if(response.status_code == 200):
                logging.info(message)
                status = True
            else:
                logging.error("Error updating file")
                
        return status

    # -----------------------------------------------------------------------------------------------------------------------------------------

    def update_comment(self,dataset_id:int,comment:str)->bool:
        """Adds comment to exisiting dataset.

        Args:
            dataset_id (int): dataset_id to add comment to.
            comment (str): comment for dataset.

        Returns:
            bool: status - True for success and False for Failure
        """
        update_comment_url = "api/updateDataSet/"
        url = db_url + update_comment_url

        data = {'data_set_id': dataset_id, 
                'comment' : comment}

        status = False
        if(self._tokenize_message(data)):
            data = json.dumps(data)
            
            # API Call
            response = requests.post(url, data=data)

            message = "Comment updated successfully for dataset id: "+str(dataset_id)+"."

            if(response.status_code == 200):
                logging.info(message)
                status = True
            else:
                logging.error("Error updating comment!")
            
        return status

    # -----------------------------------------------------------------------------------------------------------------------------------------

    def delete_dataset(self, dataset_id:int)->bool:
        """Deletes the dataset.

        Args:
            dataset_id (int): dataset id for the dataset to be deleted

        Returns:
            bool: status - True for success and False for Failure
        """
        remove_dataset_url = "api/removeDataSet/"
        url = db_url + remove_dataset_url

        data = {'data_set_id': dataset_id}
        status = False

        if(self._tokenize_message(data)):
            data = json.dumps(data)
        
            # API Call
            response1 = requests.post(url, data=data)

            # removeFSDataset FS API Call
            remove_dataset_url = "api/removeFSDataset/"
            url = db_url + remove_dataset_url

            data = {'username' : self.username,
                    'data_set_id': dataset_id}
        
            self._tokenize_message(data)
            data = json.dumps(data)
            
            # API Call
            response2 = requests.post(url, data=data)

            message = "Dataset with dataset id :"+str(dataset_id)+" deleted."

            if(response1.status_code == 200 and response2.status_code == 200):
                logging.info(message)
                status = True
            else:
                logging.error("Error deleting dataset!")

        return status

    # -----------------------------------------------------------------------------------------------------------------------------------------

    def list_dataset_files(self, dataset_id:int)->List:
        """List files present in datsets

        Args:
            dataset_id (int): Dataset id for which files are to be present.

        Returns:
            List: List of files from dataset.
        """
        list_dataset_files_url = "api/listFSDatasetFiles/"
        url = db_url + list_dataset_files_url

        data = {"username" : self.username,
                "data_set_id" : dataset_id}
        files = []
        if(self._tokenize_message(data)):
            data = json.dumps(data)
        
            # listFSDatasetFiles FS API Call
            response = requests.post(url, data=data)
            try: 
                files = (response.json()["data_set_files"])
            except:
                pass

            message = "Successfully fetched files for dataset id :"+str(dataset_id)+"."
            if(response.status_code == 200):
                logging.info(message)
                status = True
            else:
                logging.error("Error fetching files!")
            
            return files

    # -------------------------------------------------------------------------

    def get_file_from_dataset(self, dataset_id : int,file_name : str,file_save_path : str)->bool:
        """Download a file from dataset

        Args:
            dataset_id (int): Dataset ID where file is present.
            file_name (str): File name in dataset.
            file_save_path (str): Path where file needs to be saved.

        Returns:
            bool: status - True for success and False for Failure
        """
        
        get_files_url = "api/getFileFromFSDataset/"
        url = db_url + get_files_url

        data = {"username":self.username,
                "data_set_id": dataset_id,
                "file_name": file_name}

        status = False
        if(self._tokenize_message(data)):
            data = json.dumps(data)
            
            # getFileFromFSDataset FS API Call
            dataset_file_content = requests.post(url, data=data)
            if(dataset_file_content.status_code==200):
                response = HttpResponse(dataset_file_content, content_type=mimetypes.guess_type(file_name)[0])
                file_path = file_save_path+'/'+file_name
                with open(file_path, 'wb') as file_content:
                    file_content.write(response.content)
                logging.info("File has been saved at : "+str(file_path)+".")
                status = True
            else:
                logging.error("Error downloading file")

        return status
                
    # -----------------------------------------------------------------------------------------------------------------------------------------

    def list_datasets(self) -> list:
        """Lists datasets.

        Returns:
            list: List of datasets
        """
        
        dataset_url = "api/getDatasets/"
        url = db_url + dataset_url

        data = {"username":self.username}
        datasets = []
        if(self._tokenize_message(data)):
            data = json.dumps(data)
            
            # get list of datasets
            response = requests.post(url, data=data)
            
            data = response.json()
            datasets = data['Datasets']

        return datasets

    # Jobs ------------------------------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------------------------------

    def create_job(self,json_file_path:str)->int:
        """Creates a new job.

        Args:
            json_file_path (str): path for json file.

        Returns:
            int: Job id for the job created [-1 in case of failure]
        """
        with open(json_file_path, 'r') as f:
            json_file = json.load(f)
        
        data = {
            "username" : self.username,
            "dataset_id" : json_file["dataset_id"],
            "jobs_type" : json_file["type"],
            "jobs_process" : "dummy"
        }
        job_id = -1
        if(self._tokenize_message(data)):
            data = json.dumps(data)
            
            create_job_url = "api/createJob/"
            url = db_url + create_job_url 

            response1 = requests.post(url,data = data)
            data = response1.json()
            job_id = data["job_id"]
            
            # createFSJob FS API Call
            data = {
                "username" : self.username,
                "job_id" : job_id,
                "job_file_as_dict" : json_file
            }
            
            self._tokenize_message(data)

            data = json.dumps(data)
            
            create_fsjob_url = "api/createFSJob/"
            url = db_url + create_fsjob_url 
            
            response2 = requests.post(url,data = data)
            message = f"Job successfully created with id : "+str(job_id)+"."
            
            if(response1.status_code == 200 and response2.status_code == 200):
                logging.info(message)
            else:
                logging.error("Error creating job")

        return job_id

    # -----------------------------------------------------------------------------------------------------------------------------------------

    def query_job(self,job_id:int)->str:
        """Queries job status

        Args:
            job_id (int): job id

        Returns:
            str: Job status
        """

        jobs_url = "api/getJobs/"
        url = db_url + jobs_url

        data = {"username":self.username}
        
        jobs = []
        if(self._tokenize_message(data)):
            data = json.dumps(data)
            
            # get list of jobs
            response = requests.post(url, data=data)
            
            data = response.json()
            jobs = data['Jobs']

            for job in jobs:
                if (job['id']==job_id):
                    status = job['jobs_process']
                    logging.info(job['jobs_process'])

        return status
        
    # -------------------------------------------------------------------------

    def get_file_from_job(self, job_id: int, file_name: str, file_save_path : str) -> bool:
        """Download file from job

        Args:
            job_id (int): job id. 
            file_name (str): Name of the file to be downloaded.(Should be 'job.json'/'result.json')
            file_save_path (str): File path to be saved.

        Returns:
            bool: status - True for success and False for Failure
        """

        files_download_url = "api/getFileFromFSJob/"
        url = db_url + files_download_url

        data = {"username":self.username,
                "job_id": job_id,
                "file_name": file_name}

        status = False
        if(self._tokenize_message(data)):
            data = json.dumps(data)
            
            # getFileFromFSDataset FS API Call
            dataset_file_content = requests.post(url, data=data)
            if(dataset_file_content.status_code==200):
                response = HttpResponse(dataset_file_content, content_type=mimetypes.guess_type(file_name)[0])
                file_path = file_save_path+'/'+file_name
                with open(file_path, 'wb') as file_content:
                    file_content.write(response.content)
                logging.info("File : "+str(file_name)+" has been saved at : "+str(file_path)+".")
                status = True
            else:
                logging.error("Error downloading file : "+str(file_name))

        return status

    # -------------------------------------------------------------------------

    def get_files_from_job(self, job_id : int, file_save_path : str)->bool:
        """Download both 'job.json' and 'result.json' file from job

        Args:
            job_id (int): job id. 
            file_save_path (str): File path for files to be saved.

        Returns:
            bool: status - True for success and False for Failure
        """
        status = self.get_file_from_job(job_id,'job.json',file_save_path) and self.get_file_from_job(job_id,'result.json',file_save_path)
        return status
    
    # -------------------------------------------------------------------------

    def delete_job(self,job_id:int)->bool:
        """Deletes a job with given jobid

        Args:
            job_id (int): Job id to be deleted

        Returns:
            bool: status - True for success and False for Failure
        """ 
        delete_job_url = "api/removeJob/"
        url = db_url+delete_job_url

        data = {'job_id': job_id}

        status = False
        if(self._tokenize_message(data)):
            data = json.dumps(data)

            response1 = requests.post(url,data = data)
            
            # deleteFSUser FS API Call
            delete_fsjob_url = "api/removeFSJob/"
            url = db_url+delete_fsjob_url

            data = {'username': self.username,
                    'job_id': job_id}

            self._tokenize_message(data)

            data = json.dumps(data)

            response2 = requests.post(url,data = data)

            message = f"Job with job id : "+str(job_id)+" deleted successfully!"
            
            if(response1.status_code == 200 and response2.status_code == 200):
                logging.info(message)
                status = True    
            else:
                logging.error("Error deleting job")

        return status

    # -------------------------------------------------------------------------

    def list_jobs(self)->List:
        """Lists all jobs for the user.

        Returns:
            List: List all jobs of user
        """
        
        jobs_url = "api/getJobs/"
        url = db_url + jobs_url

        data = {"username":self.username}
        
        jobs = []
        if(self._tokenize_message(data)):
            data = json.dumps(data)
            
            # get list of jobs
            response = requests.post(url, data=data)
            
            data = response.json()
            jobs = data['Jobs']

        return jobs