.. _examples:

Examples
========

Installation:
*************
As the package has not been published on PyPi, it CANNOT be installed using pip.

However, Magik-eye will provide a wheel file : ``mkews_client_package.whl``. The package can be pip installed using this wheel file.

Several dependencies required by the package will also be installed while installing the package.

Python API Usage : Create datasets, create jobs and query jobs statuses
***********************************************************************

The usage and brief documentation of all the methods are present in :ref:`Pymkews_client's Python API <pymkews_client>`.

.. code-block:: python

    """This example demonstrates simple usage of the client package.
    
    - All the methods specific to datasets are included initially.

    - Methods specific to jobs are included later. 

    """
    # -*- coding: utf-8 -*-

    """
    PYMkEWS client
    """

    __author__ = "Jigar patel"
    __copyright__ = "Copyright (c) 2020, Magik-Eye Inc."
    __version__ = '1.0.0'

    # ------------------------------------------------------------------------------

    from pymkews_client.client import Client

    # ------------------------------------------------------------------------------

    cli = Client()
    cli = Client(username="test_suser_1")
    cli = Client(password="Pythonr36*")

    # Set log level 
    # 0 - INFO , 1 - ERROR
    cli.set_log_level(1)
    cli.set_log_level(0)

    # Datasets calls
    print("Datasets API: ")

    print("1. List datasets")
    print(cli.list_datasets.__doc__)
    data = cli.list_datasets()
    for row in data:
        print("datasets ",row)

    print("2. Create dataset")
    dataset_id = cli.create_dataset()
    dataset_id = cli.create_dataset(comment="Comment added while creating dataset",file_path='data/magik_eye1.png')
    dataset_id = cli.create_dataset(comment="Comment added while creating dataset")
    dataset_id = cli.create_dataset(file_path='data/magik_eye1.png')

    print("3. Update file")
    dataset_id = cli.update_file(dataset_id=120,file_path='data/magik_eye1.png')
    dataset_id = cli.update_file(dataset_id=121,file_path='data/magik_eye3.png')

    print("3. Update comment")
    status = cli.update_comment(dataset_id=115,comment="Comment updated")

    print("4. Delete dataset")
    status = cli.delete_dataset(dataset_id=118)

    print("6. List files in dataset")
    dataset_files = cli.list_dataset_files(dataset_id=114)

    print("7. Get file from dataset")
    status = cli.get_file_from_dataset(dataset_id = 119,file_name = 'magik_eye1.png',file_save_path = '/home/jigar/Desktop')

    # Jobs calls
    print()
    print("Jobs API: ")

    print("1. List Jobs")
    data = cli.list_jobs()
    for row in data:
        print("jobs ",row)

    print("2. Create Job")
    cli.create_job(json_file_path="data/job1.json")

    print("3. Delete Jobs")
    cli.delete_job(job_id=140)

    print("4. Get Job files ")
    cli.get_files_from_job(job_id = 139, file_save_path = '/home/jigar/Desktop')

    print("5. Query jobs ")
    status = cli.query_job(job_id=112)
    print(status)


Command line utility Usage : Create datasets, create jobs and query jobs statuses
*********************************************************************************

The usage and brief documentation of all the methods are present in :ref:`Pymkews_client_cli <pymkews_client_cli>`

.. code-block:: bash

    """This example demonstrates simple usage of the command line utility for the client package.
    
    - All the methods specific to datasets are included initially.

    - Methods specific to jobs are included later. 

    """
    #!/usr/bin/env bash

    # Author : Jigar Patel
    # Copyright (c) 2020, Magik-Eye Inc.

    # Log level
    # Set verbose mode ON
    pymkews_client_cli -v
    # Set quiet mode ON
    pymkews_client_cli -u test_user_1 -q

    # Datasets utility
    # 1. List datasets 
    DTSS=`pymkews_client_cli -ld -v`
    echo "List of datasets"
    echo "$DTSS"

    # 2. Create dataset
    DTSID=`pymkews_client_cli -cd -v`
    DTSID=`pymkews_client_cli --create_dataset --dataset_comment comment1`
    echo "Created dataset, $DTSID"

    # 3. Update dataset - file/comment
    `pymkews_client_cli -uc comment2 -did $DTSID`
    `pymkews_client_cli -uf "data/magik_eye1.png" -did $DTSID -v`

    # 4. Delete dataset
    `pymkews_client_cli -dd 120 -v`

    # 5. List files in dataset
    FILES=`pymkews_client_cli -ldf 121`
    echo "List of files : "
    echo "$FILES"

    # 6. get dataset files
    `pymkews_client_cli -v -gdf 'magik_eye3.png' -fsp "/home/jigar/Desktop" -did 121`

    # Jobs utility
    # 1. List jobs
    JOBS=`pymkews_client_cli -lj`
    echo "List of jobs"
    echo "$JOBS"

    # 2. Create job
    JOBID=`pymkews_client_cli -cj "data/job1.json"`
    echo "Jobs created with job id : $JOBID"

    # 3. Delete job
    `pymkews_client_cli -dj 149 -v`

    # 4. Get job files
    `pymkews_client_cli -v -gjf -jid 147 -fsp '/home/jigar/Desktop'`

    # 5. Query jobs
    STATUS=`pymkews_client_cli -qj 145 -v`
    echo "$STATUS"
