.. _INTRODUCTION:

INTRODUCTION
============

**Pymkews client package** is a `Python <http://www.python.org>`_ package which aims to provide an easy and intuitive way of interacting with the `Magik eye Calibration server <http://cluster.magik-eye.cz:8080/>`_. 
In essence, this package is an extension of the Website and can be used for automations. (see :ref:`examples <examples>`)

The aim here was to define a single object which would allow users to perform the various operations like creating, deleting and editing datasets and jobs.

About the package
-----------------

List of methods and input/output parameters can be checked in :ref:`pymkews client package<pymkews_client_package>` section.
Installation and Usage example can be found in :ref:`Examples <examples>` section.

The example for creating datasets, creating jobs and querying the job status have been included in the :ref:`Examples <examples>` section.

Limitations
-----------
Creating users, Deleting users and other admin related functionalities are not a part of this package.
