.. _pymkews_client_cli:

Command Line Utility
====================

PYMkEWS command line client

Usage: pymkews_client_cli [-h] [-u] [-p] [-q | -v] [-ld] [-cd] [-dc] [-df]
                          [-uc] [-uf] [-did] [-dd] [-ldf] [-gdf] [-fsp] [-lj]
                          [-cj] [-dj] [-gjf] [-jid] [-qj]

Pymkews client package.

Optional arguments
          
| **-h, --help**                       -> shows this help message and exit
| **-u , --username**                  -> Username to access the package
| **-p , --password**                  -> Password to access the package

| **-q, --quiet**                      -> Quiet mode - Only errors will be visible if the quiet flag is set.
| **-v, --verbose**                    -> Verbose mode - Errors as well as other logging info will be visible if the verbose flag is set.

| **-ld, --list_datasets**             -> Lists datasets.

| **-cd, --create_dataset**            -> Create Dataset - Provide `dataset_id`. (`dataset_comment` and `dataset_file` optional flags can be used)
| **-dc , --dataset_comment**          -> Add comment to dataset.
| **-df , --dataset_file**             -> Add file to dataset.

| **-uc , --update_comment**           -> Update Comment. Provide `dataset_id` to update the comment to.
| **-uf , --update_file**              -> Update File. Provide `dataset_id` to update the file to.
| **-did , --dataset_id**              -> Dataset id.

| **-dd , --delete_dataset**           -> Delete a datset using dataset id.

| **-ldf , --list_datasetfiles**       -> List files present in the dataset.
| **-gdf , --get_dataset_file**        -> Download files present in the dataset. Provide `file_save_path` and `dataset_id`.
| **-fsp , --file_save_path**          -> File save path parameter.

| **-lj, --list_jobs**                 -> Lists jobs.

| **-cj , --create_job**               -> Provide jobs json file path for creating job. Returns job id for the job created.

| **-dj , --delete_job**               -> Delete a job using job id.

| **-gjf, --get_job_files**            -> Download job files. Provide `file_save_path` and `job_id` parameters.

| **-jid , --job_id**                  -> Job id.

| **-qj , --query_job**                -> Query the job using job_id.
