#!/usr/bin/env python
"""connecto application specific URL Configuration"""

from django.conf.urls import url, include
from django.urls import path
from connecto import views, api
from users import views as user_views


urlpatterns = [
    path('', views.home, name="home"),
    path('home/', views.home, name="home"),
    path('jobs/', views.jobs, name="jobs"),
    path('adminView/', views.admin_view, name="admin-view"),
    url(r'^loginAs/user/(?P<user_name>.*)$', views.admin_as_user_login),

    path('^dataSetDetails/(?P<data_set_id>\d+)$', views.dataset_details, name="dataSetDetails"),
    path(r'viewDatasetfile/(?P<data_set_id>\d+)/(?P<file_name>\d+)$', user_views.view_dataset_file, name="viewDatasetfile"),
    path(r'downloadDatasetfile/(?P<data_set_id>\d+)/(?P<file_name>\d+)$', user_views.download_dataset_file, name="downloadDatasetfile"),

    path('^jobDetails/(?P<job_id>\d+)$', views.job_details, name="jobDetails"),
    path(r'viewjobfile/(?P<job_id>\d+)/(?P<file_name>\d+)$', user_views.view_job_file, name="viewjobfile"),
    path(r'downloadjobfile/(?P<job_id>\d+)/(?P<file_name>\d+)$', user_views.download_job_file, name="downloadjobfile"),

    path('home/createDataSets', user_views.create_datasets, name="createDataSets"),
    path('home/uploadfilesforDS', user_views.upload_files_for_DS, name="uploadfilesforDS"),
    path('^dataSetDetails/uploadfilesforDSfromDSD', user_views.upload_files_for_DS_from_DSD, name="uploadfilesforDSfromDSD"),
    path('home/updatecommentforDS', user_views.update_comment_for_DS, name="updatecommentforDS"),
    path('home/removeDataset', user_views.remove_dataset, name="removeDataset"),

    path('createDataSets', user_views.create_datasets, name="createDataSets"),
    path('uploadfilesforDS', user_views.upload_files_for_DS, name="uploadfilesforDS"),
    path('updatecommentforDS', user_views.update_comment_for_DS, name="updatecommentforDS"),
    path('removeDataset', user_views.remove_dataset, name="removeDataset"),

    path('jobs/createJob', user_views.create_job, name="createJob"),
    path(r'^jobs/refreshJob/(?P<job_id>\d+)/(?P<process>\d+)$', user_views.refresh_job, name="refreshJob"),
    path('jobs/removeJob', user_views.remove_job, name="removeJob"),

    path('createJob', user_views.create_job, name="createJob"),
    path('removeJob', user_views.remove_job, name="removeJob"),
    path(r'^refreshJob/(?P<job_id>\d+)/(?P<process>\d+)$', user_views.refresh_job, name="refreshJob"),

    # API's

    path('api/login/', api.login),
    path('api/getsecretToken/', api.getsecretToken),


]
