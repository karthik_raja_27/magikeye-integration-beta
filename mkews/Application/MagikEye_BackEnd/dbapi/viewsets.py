#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    MagikEye Viewsets Definition's For Datasets and Jobs
"""
__author__ = "Jigar Patel" 
__e_mail__ = "jigar@magikeye.com" 
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ----------------------------------------------------------------------------------------------------------------------

from rest_framework import viewsets
from . import models
from . import serializers

class DatasetsViewset(viewsets.ModelViewSet):
    queryset = models.Datasetsdata.objects.all()
    serializer_class = serializers.DatasetsSerializer

class JobsViewset(viewsets.ModelViewSet):
    queryset = models.Jobsdata.objects.all()
    serializer_class = serializers.JobsSerializer

