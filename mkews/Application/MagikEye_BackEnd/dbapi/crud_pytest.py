#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" CRUD functionality test cases for API (datasets and jobs objects) using pytest """
__author__ = "Kiran N V"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# -----------------------------------------------------------------------------

import pytest
import requests
import json
import random
datasets_start_id = 0
jobs_start_id = 0

# ------------------------------------------------------------------------------------------------

def test_read_datasets():
    url = "http://127.0.0.1:8002/api/datasets/"
    response = requests.get(url)
    assert response.status_code == 200
    datasets_id = []
    
    
    for dataset in response.json():
        datasets_id.append(dataset['id'])
        assert type(dataset['id']) == int
        assert type(dataset['dataset_user']) == str
        assert type(dataset['dataset_date_comment']) == str
        assert type(dataset['dataset_date_created']) == str
        
    global datasets_start_id 
    datasets_start_id = len(datasets_id)
    
# -------------------------------------------------------------------------------------------------

@pytest.mark.parametrize ("dataset_user, dataset_date_created, dataset_date_comment, status",
[('admin','2020-08-06T15:12:05.058970Z','pytest',201),
('ad12','2020-08-06T15:12:05.058970Z','pytest12',201),
('123','2020-08-06T15:12:05.058970Z','123',201),
(None, None, None, 400),
('admin', None, None, 400),
('admin','2020-08-06T15:12:05.058970Z',None, 400),
('admin','2020-08-06','pytest',400)])

def test_create_datasets(dataset_user, dataset_date_created, dataset_date_comment, status):
    url = "http://127.0.0.1:8002/api/datasets/"
    data = {'dataset_user': dataset_user,'dataset_date_created': dataset_date_created,'dataset_date_comment': dataset_date_comment}
    response = requests.post(url, data=data)
    assert response.status_code == status

# ----------------------------------------------------------------------------------------------------

@pytest.mark.parametrize ("dataset_user, dataset_date_created, dataset_date_comment, status",
[('kiran','2020-08-06T15:12:05.058970Z','checking update',200),
('jigar','2020-08-06T15:12:05.058970Z','updated',200),
(None, None, None, 400),
('admin', None, None, 400),
('admin','2020-08-06T15:12:05.058970Z',None, 400),
('admin','2020-08-06','pytest',400)])

def test_update_datasets(dataset_user, dataset_date_created, dataset_date_comment, status):
    url = "http://127.0.0.1:8002/api/datasets/"
    response = requests.get(url)
    datasets_id = []
    for dataset in response.json():
        datasets_id.append(dataset['id']) # get the ids
    
    start_id = datasets_id[datasets_start_id:]
    random_update = random.choice(start_id)
    data = {'dataset_user': dataset_user,'dataset_date_created': dataset_date_created,'dataset_date_comment': dataset_date_comment}    
    response = requests.put(url+str(random_update)+'/', data=data)
    assert response.status_code == status

# ---------------------------------------------------------------------------------------------------------------

def test_delete_datasets():
    url = "http://127.0.0.1:8002/api/datasets/"
    response = requests.get(url)
    datasets_id =[]
    for dataset in response.json():
        datasets_id.append(dataset['id'])
    
    if datasets_start_id != 0:
        start_id = datasets_id[datasets_start_id:]     
        
        for dataset in start_id:
            response = requests.delete(url+str(dataset)+'/')
            assert response.status_code == 204

# ---------------------------------------------------------------------------------------------------------------

def test_read_jobs():
    url = "http://127.0.0.1:8002/api/jobs/"
    response = requests.get(url)
    assert response.status_code == 200
    jobs_id = []
    for job in response.json():
        jobs_id.append(job['id'])   # get the ids
        assert type(job['id']) == int
        assert type(job['jobs_user']) == str
        assert type(job['jobs_dataset_id']) == str
        assert type(job['jobs_date_created']) == str
        assert type(job['jobs_type']) == str
        assert type(job['jobs_process']) == str
    global jobs_start_id 
    jobs_start_id = len(jobs_id)
    
# ---------------------------------------------------------------------------------------------------------------

@pytest.mark.parametrize("jobs_user, jobs_dataset_id, jobs_date_created, jobs_type, jobs_process, status",
[('admin','12','2020-08-06T15:12:05.058970Z','type1','process1',201),
('ad12','15','2020-08-06T15:12:05.058970Z','type2','pytest12',201),
('123','16','2020-08-06T15:12:05.058970Z','123','456',201),
(None, None, None, None, None, 400),
('admin', None, None, None, None, 400),
('admin','12','2020-08-06T15:12:05.058970Z', None, None, 400),
('admin','11','2020-08-06T15:12:05.058970Z', 'type5', None, 400),
('admin','0','2020-08-06', 'type5', 'process6', 400)])

def test_create_jobs(jobs_user, jobs_dataset_id, jobs_date_created, jobs_type, jobs_process, status):
    url = "http://127.0.0.1:8002/api/jobs/"
    
    data = {'jobs_user': jobs_user, 'jobs_dataset_id': jobs_dataset_id,'jobs_date_created': jobs_date_created, 
    'jobs_type': jobs_type, 'jobs_process': jobs_process,'status': status}
    
    response = requests.post(url,data=data)
    assert response.status_code == status

# -----------------------------------------------------------------------------------------------------------------

@pytest.mark.parametrize("jobs_user, jobs_dataset_id, jobs_date_created, jobs_type, jobs_process, status",
[('kiran','12','2020-08-06T15:12:05.058970Z','type100','process100',200),
('jigar','60','2020-08-06T15:12:05.058970Z','type99','process88',200),
(None, None, None, None, None, 400),
('admin', None, None, None, None, 400),
('admin','35','2020-08-06T15:12:05.058970Z', None, None, 400),
('admin','64','2020-08-06T15:12:05.058970Z', 'type5', None, 400),
('admin','41','2020-08-06', 'type5', 'process6', 400)])

def test_update_jobs(jobs_user, jobs_dataset_id, jobs_date_created, jobs_type, jobs_process, status):
    url = "http://127.0.0.1:8002/api/jobs/"
    response = requests.get(url)
    jobs_id = []
    
    for job in response.json():
        jobs_id.append(job['id']) # get the ids
    
    start_id = jobs_id[jobs_start_id:]
    random_update = random.choice(start_id)
    data = {'jobs_user': jobs_user, 'jobs_dataset_id': jobs_dataset_id,'jobs_date_created': jobs_date_created, 
    'jobs_type': jobs_type, 'jobs_process':jobs_process,'status': status}  
    
    response = requests.put(url+str(random_update)+'/', data=data)
    assert response.status_code == status

# -------------------------------------------------------------------------------------------------------------

def test_delete_jobs():
    url = "http://127.0.0.1:8002/api/jobs/"
    response = requests.get(url)
    jobs_id =[]
    for job in response.json():
        jobs_id.append(job['id'])
    
    if jobs_start_id != 0:
        start_id = jobs_id[jobs_start_id:]     
        
        for job in start_id:
            response = requests.delete(url+str(job)+'/')
            assert response.status_code == 204

# --------------------------------------------------------------------------------------------------------------
