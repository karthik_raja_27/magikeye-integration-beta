#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    MagikEye Database Model Definition's For Storing Datasets and Jobs in DataBase
"""
__author__ = "Jigar Patel" 
__e_mail__ = "jigar@magikeye.com" 
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ----------------------------------------------------------------------------------------------------------------------

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Datasetsdata(models.Model):
    """
    :Author:  Jigar Patel
    :Description: Model definition for datasets for a given user
    :Model Fields: ‘ID’, ‘USER’, ‘DATE_CREATED’, ‘COMMENT’
    """
    
    dataset_user = models.CharField(max_length=100)
    dataset_date_created = models.DateTimeField(default = timezone.now)
    dataset_date_comment = models.CharField(max_length=100)
    
# ----------------------------------------------------------------------------------------------------------------------

class Jobsdata(models.Model):
    """
    :Author:  Jigar Patel
    :Description: Model definition for jobs for a given user
    :Model Fields: ‘ID’, ‘USER’, ‘DATASET_ID’, ‘DATE_CREATED’, ‘TYPE’, ‘PROCESS_ID’
    """

    jobs_user = models.CharField(max_length=100)
    jobs_dataset_id = models.CharField(max_length=100)
    jobs_date_created = models.DateTimeField(default = timezone.now)
    jobs_type = models.CharField(max_length=100)
    jobs_process = models.CharField(max_length=100)


class AuthToken(models.Model):
    username = models.CharField(max_length=100)
    secret_token = models.TextField()

    def __str__(self):
        return f"{self.username}'s Token"