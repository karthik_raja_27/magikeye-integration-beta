# CMake for MkEWS
# 2020, Jan Heller, jan@magik-eye.com

CMAKE_MINIMUM_REQUIRED(VERSION 3.10)
PROJECT(mkews)

OPTION(BUILD_MKEWS_CONTROLLER "Build MkEWS controller wheel" ON)
OPTION(BUILD_MKEWS_WORKER "Build MkEWS worker wheel" ON)
OPTION(BUILD_MKEWS_CLIENT "Build MkEWS client wheel" ON)

################################################################################

SET(MKEWS_VERSION_MAJOR 1)
SET(MKEWS_VERSION_MINOR 0)
SET(MKEWS_VERSION_PATCH 0)

################################################################################

SET(WHEELS_ROOT "${CMAKE_CURRENT_BINARY_DIR}/pypi/index")

################################################################################

IF(NOT PYTHON_VERSION)
  SET(PYTHON_VERSION 3)
ENDIF()

SET(Python_ADDITIONAL_VERSIONS ${PYTHON_VERSION})
FIND_PACKAGE(PythonInterp ${PYTHON_VERSION} EXACT)
FIND_PACKAGE(PythonLibs ${PYTHON_VERSION} EXACT REQUIRED)


STRING(REGEX REPLACE "\\.[^.]*$" "" PYTHONLIBS_VERSION_MAJMIN "${PYTHONLIBS_VERSION_STRING}")

################################################################################


IF (BUILD_MKEWS_WORKER)
    ADD_SUBDIRECTORY(pymkews_worker)
ENDIF()

IF (BUILD_MKEWS_CONTROLLER)
    ADD_SUBDIRECTORY(pymkews_controller)
ENDIF()

IF (BUILD_MKEWS_CLIENT)
    ADD_SUBDIRECTORY(pymkews_client)
ENDIF()