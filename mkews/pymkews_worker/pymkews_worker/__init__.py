# -*- coding: utf-8 -*-

"""
MkEWS worker
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."
__version__ = '@PYMKEWS_WORKER_VERSION@'

#from .worker import *
#from .error import Error
