#!/usr/bin/env bash

BASEDIR=$(dirname "$0")

export PYTHONPATH=`realpath $BASEDIR/..`
exec python3 -m pymkews_worker.worker $@
