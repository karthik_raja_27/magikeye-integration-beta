# -*- coding: utf-8 -*-

"""
MkEWS Worker
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

import os
from typing import List, IO
from fs import open_fs
import logging


# ------------------------------------------------------------------------------

class DataStorage:

    def __init__(self, job_url: str, dts_url: str):
        self.job_url = job_url
        self.dts_url = dts_url
        self.logger = logging.getLogger('pymkews.worker.data_storage')

    #def list_dts_files(self): -> List[str]:

    # -------------------------------------------------------------------------

    def add_data_to_job(self, file_name: str, file_data: bytes) -> None:
        fs = open_fs(self.job_url)

        file_name = os.path.basename(file_name)
        with fs.openbin(file_name, 'w') as file:
            file.write(file_data)

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Added file to job: {file_name}')

    # -------------------------------------------------------------------------

    def get_file_from_job(self, file_name: str) -> IO:
        fs = open_fs(self.job_url)
        fd = fs.openbin(file_name)

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Opened file in job: {file_name}')

        return fd

    # -------------------------------------------------------------------------

    def copy_files_to_job(self, file_list: str):
        if os.path.isdir(self.dts_url):
            pass
        else:
            # TODO
            pass

    # -------------------------------------------------------------------------

    def get_job_path(self):
        if os.path.isdir(self.job_url):
            return self.job_url
        else:
            # TODO
            pass

    # -------------------------------------------------------------------------

    def add_data_to_dts(self, file_name: str, file_data: bytes) -> None:
        fs = open_fs(self.dts_url)

        file_name = os.path.basename(file_name)
        with fs.openbin(file_name, 'w') as file:
            file.write(file_data)

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Added file to dts: {file_name}')

    # -------------------------------------------------------------------------

    def get_dts_path(self):
        if os.path.isdir(self.dts_url):
            return self.dts_url
        else:
            # TODO
            pass

# ------------------------------------------------------------------------------
