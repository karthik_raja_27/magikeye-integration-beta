#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
MkEWS Worker
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

import logging
import json
import argparse

from pymkews_worker.jobs.registry import JOB_TYPE_REGISTRY
from pymkews_worker.error import Error
from pymkews_worker.data_storage import DataStorage


# ------------------------------------------------------------------------------

class JobValidator:

    @staticmethod
    def validate(job_json: dict) -> (str):
        job_type = ''

        try:
            job_type = job_json["type"] + "-" + job_json["version"]

            if job_type not in JOB_TYPE_REGISTRY:
                raise Error(f'Not a valid job type: {job_json["type"]}')

            if 'params' not in job_json:
                raise Error(f'Missing "params" property')

        except Exception as err:
            raise Error('Cannot validate job JSON: ' + str(err))

        return job_type


# ------------------------------------------------------------------------------

def main_func():
    args = argparse.ArgumentParser(prog='pymkews_worker',
                                   description='MkEWS Worker, (c) 2020 MagikEye Inc.')
    args.add_argument('-j', '--job_url', type=str, default='',
                      help='Job URL')
    args.add_argument('-d', '--dts_url', type=str, default='',
                      help='Dataset URL')

    opts = args.parse_args()

    if not opts.job_url:
        raise RuntimeError('"job_url" parameter not set')

    data_storage = DataStorage(opts.job_url, opts.dts_url)

    results = {'error_message': '',
               'status': 'failed',
               'files': []}

    try:
        # Load job.json
        with data_storage.get_file_from_job('job.json') as f:
            job_json = json.load(f)

        # Validate job
        job_type = JobValidator.validate(job_json)

        # Create job object
        job = JOB_TYPE_REGISTRY[job_type]()

        # Execute job
        res_files = job.execute(job_json, data_storage)

        # Copy files to job URL (if not local filesystem)
        data_storage.copy_files_to_job(res_files)
        results['files'] = res_files

        # Mark as successful execution
        results['status'] = 'success'
    except Exception as err:
        results['error_message'] = str(err)

    data_storage.add_data_to_job('results.json', json.dumps(results, indent=2).encode('utf-8'))


# ------------------------------------------------------------------------------

if __name__ == "__main__":
    # create logger
    logger = logging.getLogger('pymkews.worker')
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

    main_func()

