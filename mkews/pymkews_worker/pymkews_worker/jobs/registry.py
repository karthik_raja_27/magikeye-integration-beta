# -*- coding: utf-8 -*-

"""
MkEWS Worker
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

from .mkecc_v10 import MkeccV10Job
from .sleep_v10 import SleepV10Job

# ------------------------------------------------------------------------------

JOB_TYPE_REGISTRY = \
    {
        'mkecc-1.0': MkeccV10Job,
        'sleep-1.0': SleepV10Job
    }
