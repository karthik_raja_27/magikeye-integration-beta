# -*- coding: utf-8 -*-

"""
MkEWS Worker
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

from abc import ABC, abstractmethod
from pymkews_worker.data_storage import DataStorage
from typing import List


# ------------------------------------------------------------------------------

class Job(ABC):
    @abstractmethod
    def execute(self, job_json: dict, data_storage: DataStorage) -> List[str]:
        pass
