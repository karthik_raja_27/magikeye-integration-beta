# -*- coding: utf-8 -*-

"""
MkEWS Data Storage
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

import logging
import os
from typing import List, IO
from fs import open_fs

from ..error import Error
from .data_storage import DataStorage


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

class PyFsDataStorage(DataStorage):

    def __init__(self, config):
        self.config = config
        self.logger = logging.getLogger('pymkews.data_storage')

        root = config['root']
        # Test fs accessibility
        fs = open_fs(root)

    # -------------------------------------------------------------------------

    @staticmethod
    def __get_dts_dir(dts_id=-1):
        dts_dir = 'datasets'

        if dts_id >= 0:
            dts_dir += dts_id

        return dts_dir

    # -------------------------------------------------------------------------

    @staticmethod
    def __get_job_dir(job_id=-1):
        job_dir = 'jobs'

        if job_id >= 0:
            job_dir += job_id

        return job_dir

    # -------------------------------------------------------------------------

    def __get_fs(self, user_name='', job_dir=False, dts_dir=False):
        root = self.config['root']

        if not user_name:
            return open_fs(root)
        else:
            root += '/' + user_name

        if dts_dir:
            root += '/' + self.__get_dts_dir()
        elif job_dir:
            root += '/' + self.__get_job_dir()

        return open_fs(root)

    # -------------------------------------------------------------------------
    # Users -------------------------------------------------------------------

    def create_user(self, user_name: str) -> str:
        fs = self.__get_fs()
        user_fs = fs.makedir(user_name)
        user_fs.makedir(self.__get_job_dir())
        user_fs.makedir(self.__get_dts_dir())

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Created user: {user_name}, {user_fs}')

        return user_fs.getsyspath('/')

    # -------------------------------------------------------------------------

    def delete_user(self, user_name: str) -> None:
        fs = self.__get_fs()
        fs.removetree(user_name)

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Removed user: {user_name}')

    # -------------------------------------------------------------------------

    def list_users(self) -> List[int]:
        fs = self.__get_fs()
        users = fs.listdir('/')

        if self.logger.isEnabledFor(logging.INFO):
            no_users = len(users)
            self.logger.info(f'Listed {no_users} users')

        return users

    # -------------------------------------------------------------------------
    # Datasets ----------------------------------------------------------------

    def create_dataset(self, user_name: str, dataset_id: int) -> str:
        if dataset_id == DataStorage.DEFAULT_DATASET_ID:
            raise Error(f'Cannot create dataset, "{dataset_id}" is not a valid dataset ID')

        fs = self.__get_fs(user_name, dts_dir=True)
        dts_fs = fs.makedirs(str(dataset_id))

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Created dataset: {dataset_id}, {dts_fs}')

        return dts_fs.getsyspath('/')

    # -------------------------------------------------------------------------

    def get_dataset_url(self, user_name: str, dataset_id: int, exists: bool = False) -> str:
        fs = self.__get_fs(user_name, dts_dir=True)
        url = fs.getsyspath(str(dataset_id))

        if exists:
            if not fs.exists(str(dataset_id)):
                raise Error(f'Dataset URL does not exist: {url}')

        return url

    # -------------------------------------------------------------------------

    def add_file_to_dataset(self, user_name: str, dataset_id: int, file_path: str) -> None:
        if not os.path.exists(file_path):
            raise Error(f'Cannot add to dataset, file does not exist: {file_path}')

        fs = self.__get_fs(user_name, dts_dir=True)
        dts_fs = fs.opendir(str(dataset_id))

        file_name = os.path.basename(file_path)
        with open(file_path, 'rb') as file:
            dts_fs.upload(file_name, file)

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Added file to dataset: {dataset_id}, {file_path}')

    # -------------------------------------------------------------------------

    def get_file_from_dataset(self, user_name: str, dataset_id: int, file_name: str) -> IO:
        file_list = self.list_dataset_files(user_name, dataset_id)

        fs = self.__get_fs(user_name, dts_dir=True)
        dts_fs = fs.opendir(str(dataset_id))

        if file_name in file_list:
            if self.logger.isEnabledFor(logging.INFO):
                self.logger.info(f'Served file from dataset: {dataset_id}, {file_name}')

            return dts_fs.openbin(file_name)
        else:
            raise Error(f'Cannot get a file, not in dataset ID {dataset_id}: {file_name}')

    # -------------------------------------------------------------------------

    def list_dataset_files(self, user_name: str, dataset_id: int) -> List[str]:
        fs = self.__get_fs(user_name, dts_dir=True)
        return fs.listdir(str(dataset_id))

    # -------------------------------------------------------------------------

    def delete_dataset(self, user_name: str, dataset_id: int) -> None:
        fs = self.__get_fs(user_name, dts_dir=True)
        fs.removetree(str(dataset_id))

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Deleted dataset: {dataset_id}, {fs}/{dataset_id}')

    # -------------------------------------------------------------------------

    def list_datasets(self, user_name: str) -> List[int]:
        fs = self.__get_fs(user_name, dts_dir=True)
        dts = fs.listdir('/')

        if self.logger.isEnabledFor(logging.INFO):
            no_dts = len(dts)
            self.logger.info(f'Listed {no_dts} datasets')

        return [int(dts_id) for dts_id in dts]

    # -------------------------------------------------------------------------
    # Jobs --------------------------------------------------------------------

    def create_job(self, user_name: str, job_id: int) -> str:
        fs = self.__get_fs(user_name, job_dir=True)
        job_fs = fs.makedirs(str(job_id))

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Created job: {job_id}, {job_fs}')

        return job_fs.getsyspath('/')

    # -------------------------------------------------------------------------

    def get_job_url(self, user_name: str, job_id: int) -> str:
        fs = self.__get_fs(user_name, job_dir=True)
        return fs.getsyspath(str(job_id))

    # -------------------------------------------------------------------------

    def add_data_to_job(self, user_name: str, job_id: int, file_name: str, file_data: bytes) -> None:
        if not file_name:
            raise Error('Cannot add data to job, file name not provided')

        fs = self.__get_fs(user_name, job_dir=True)
        job_fs = fs.opendir(str(job_id))

        file_name = os.path.basename(file_name)
        with job_fs.openbin(file_name, 'w') as file:
            file.write(file_data)

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Added file to job: {job_id}, {file_name}')

    # -------------------------------------------------------------------------

    def get_file_from_job(self, user_name: str, job_id: int, file_name: str) -> IO:
        file_list = self.list_job_files(user_name, job_id)

        fs = self.__get_fs(user_name, job_dir=True)
        job_fs = fs.opendir(str(job_id))

        if file_name in file_list:
            if self.logger.isEnabledFor(logging.INFO):
                self.logger.info(f'Served file from job: {job_id}, {file_name}')

            return job_fs.openbin(file_name)
        else:
            raise Error(f'Cannot get a file, not in job ID {job_id}: {file_name}')

    # -------------------------------------------------------------------------

    def list_job_files(self, user_name: str, job_id: int) -> List[str]:
        fs = self.__get_fs(user_name, job_dir=True)
        return fs.listdir(str(job_id))

    # -------------------------------------------------------------------------

    def delete_job(self, user_name: str, job_id: int) -> None:
        fs = self.__get_fs(user_name, job_dir=True)
        job_fs = fs.removetree(str(job_id))

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Deleted job: {job_id}, {job_fs}')

    # -------------------------------------------------------------------------

    def list_jobs(self, user_name: str) -> List[int]:
        fs = self.__get_fs(user_name, job_dir=True)
        jobs = fs.listdir('/')

        if self.logger.isEnabledFor(logging.INFO):
            no_jobs = len(jobs)
            self.logger.info(f'Listed {no_jobs} jobs')

        return [int(job_id) for job_id in jobs]
