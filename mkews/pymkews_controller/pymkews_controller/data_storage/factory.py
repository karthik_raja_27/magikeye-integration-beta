# -*- coding: utf-8 -*-

"""
MkEWS Data Storage
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

from mkews.pymkews_controller.pymkews_controller.error import Error
from .pyfs import PyFsDataStorage


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

class DataStorageFactory:

    @staticmethod
    def create(config: dict):

        if 'type' not in config:
            raise Error('Unknown DataStorage type, please specify "type" property in the configuration')

        if 'params' not in config:
            raise Error('Missing "param" property in DataStorage configuration')

        if config['type'] == 'osfs' or config['type'] == 'pyfs':
            return PyFsDataStorage(config['params'])
        else:
            raise Error(f'Unknown JobExecutor type: {config["type"]}')
