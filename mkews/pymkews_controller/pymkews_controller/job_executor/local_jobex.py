# -*- coding: utf-8 -*-

"""
MkEWS Job Executor
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

from typing import Optional
import logging
import json
import subprocess
import psutil
import signal
import os

from ..data_storage import DataStorage
from .job_executor import JobExecutor
from .job_validator import JobValidator


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

class LocalJobExecutor(JobValidator, JobExecutor):

    def __init__(self, config: dict, data_storage: DataStorage) -> None:
        self.config = config
        self.data_storage = data_storage
        self.logger = logging.getLogger('pymkews.job_executor.local')
        self.processes = {}

    # -------------------------------------------------------------------------

    def validate_job(self, user_name: str, job_json_path: str = '', job_json: Optional[dict] = None) -> (str, dict):
        job_type, job_json = super().validate(user_name, self.data_storage,
                                              job_json_path=job_json_path,
                                              job_json=job_json)

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Validated Job type: {job_type}')

        return job_type, job_json

    # -------------------------------------------------------------------------

    def execute_job(self, user_name: str, job_id: int, job_json: dict) -> None:
        # Create new job in the data storage
        job_url = self.data_storage.create_job(user_name, job_id)

        if 'dataset_id' in job_json:
            dts_url = self.data_storage.get_dataset_url(user_name, job_json['dataset_id'])
        else:
            dts_url = ''

        process_info = {
            'job_url': job_url,
            'dts_url': dts_url,
            'job_id': job_id,
            'job': job_json
        }

        self.data_storage.add_data_to_job(user_name, job_id, 'job.json', json.dumps(job_json, indent=2).encode('utf-8'))

        args = [self.config['exec'], '-j', job_url]

        if dts_url:
            args += ['-d', dts_url]

        sp = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        process_info['sp'] = sp
        self.processes[job_id] = process_info

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Executed process, job_id:{job_id}, pid:{sp.pid}')

    # -------------------------------------------------------------------------

    def query_job(self, user_name: str, job_id: int) -> (int, dict):
        remove_process = True
        results = {}

        if job_id in self.processes:
            sp = self.processes[job_id]['sp']
            pid = sp.pid

            try:
                p = psutil.Process(pid)

                if p.status() != 'zombie':
                    remove_process = False
                else:
                    if self.logger.isEnabledFor(logging.INFO):
                        self.logger.info(f'Process is a zombie, job_id:{job_id}, pid:{pid}')

            except psutil.NoSuchProcess:
                if self.logger.isEnabledFor(logging.INFO):
                    self.logger.info(f'No such process, job_id:{job_id}')
            finally:
                if remove_process:
                    self.processes.pop(job_id)
                    sp.send_signal(signal.SIGINT)
                    sp.wait()
                    if self.logger.isEnabledFor(logging.INFO):
                        lines = sp.stderr.read().decode('utf-8').split(os.linesep)
                        for line in lines:
                            self.logger.info(line)
                else:
                    return self.PROCESS_RUNNING, results

        # Process is not running, that is the exit status?
        jfiles = self.data_storage.list_job_files(user_name, job_id)

        if 'results.json' not in jfiles:
            results['error_message'] = 'Unknown error (No error message returned)'
            return self.PROCESS_FAILURE, results

        try:
            with self.data_storage.get_file_from_job(user_name, job_id, 'results.json') as f:
                results = json.load(f)
                error_message = results['error_message']

                if results['status'] == 'failed':
                    return self.PROCESS_FAILURE, results

                return self.PROCESS_SUCCESS, results
        except Exception:
            results['error_message'] = 'Unknown error (No error message returned)'
            return self.PROCESS_FAILURE, results

    # -------------------------------------------------------------------------

    def delete_job(self, user_name: str, job_id: int) -> None:
        # Kill process
        if job_id in self.processes:
            sp = self.processes[job_id]['sp']
            self.processes.pop(job_id)
            sp.send_signal(signal.SIGINT)
            sp.wait()

            if self.logger.isEnabledFor(logging.INFO):
                self.logger.info(f'Killed process: job_id:{job_id}')
        else:
            if self.logger.isEnabledFor(logging.INFO):
                self.logger.info(f'Cannot kill process, pid does not exist: {job_id}')

        # Delete job
        self.data_storage.delete_job(user_name, job_id)

