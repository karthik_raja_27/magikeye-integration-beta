# -*- coding: utf-8 -*-

"""
MkEWS Job Executor
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

from typing import Optional
import logging
import json
from random import random

from ..data_storage import DataStorage
from .job_executor import JobExecutor
from .job_validator import JobValidator


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

class TestingJobExecutor(JobValidator, JobExecutor):

    def __init__(self, config: dict, data_storage: DataStorage) -> None:
        self.config = config
        self.data_storage = data_storage
        self.logger = logging.getLogger('pymkews.job_executor.testing')
        self.processes = {}
        self.processes_id = 0

    # -------------------------------------------------------------------------

    def validate_job(self, user_name: str, job_json_path: str = '', job_json: Optional[dict] = None) -> (str, dict):
        job_type, job_json = super().validate(user_name, self.data_storage,
                                              job_json_path=job_json_path,
                                              job_json=job_json)

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Validated Job type: {job_type}')

        return job_type, job_json

    # -------------------------------------------------------------------------

    def execute_job(self, user_name: str, job_id: int, job_json: dict) -> None:
        # Create new job in the data storage
        job_url = self.data_storage.create_job(user_name, job_id)
        dts_url = self.data_storage.get_dataset_url(user_name, job_json['dataset_id'])

        process_info = {
            'job_url': job_url,
            'dts_url': dts_url,
            'job_id': job_id,
            'job': job_json
        }

        self.processes_id += 1
        self.processes[job_id] = process_info

        self.data_storage.add_data_to_job(user_name, job_id, 'job.json', json.dumps(job_json, indent=2).encode('utf-8'))

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Executed process, job_id:{job_id}')

    # -------------------------------------------------------------------------

    def query_job(self, user_name: str, job_id: int) -> (int, dict):
        if job_id not in self.processes:
            return self.PROCESS_UNKNOWN

        t = self.config['execution_rate']
        fr = self.config['failure_rate']

        results = {}

        if random() < t:
            self.processes.pop(job_id)

            if random() < fr:
                results['error_message'] = 'Process error message'
                results['status'] = 'failure'
                return self.PROCESS_FAILURE, results

            results['status'] = 'success'
            results['error_message'] = ''
            return self.PROCESS_SUCCESS, results

        else:
            results['status'] = 'running'
            results['error_message'] = ''
            return self.PROCESS_RUNNING, results

    # -------------------------------------------------------------------------

    def delete_job(self, user_name: str, job_id: int) -> None:
        # Kill process
        if job_id in self.processes:
            p = self.processes[job_id]
            self.processes.pop(job_id)

            if self.logger.isEnabledFor(logging.INFO):
                job_id = p['job_id']
                self.logger.info(f'Killed process: job_id:{job_id}')
        else:
            if self.logger.isEnabledFor(logging.INFO):
                self.logger.info(f'Cannot kill process, does not exist: {job_id}')

        # Delete job
        self.data_storage.delete_job(user_name, job_id)
