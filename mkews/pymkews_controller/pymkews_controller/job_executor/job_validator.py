# -*- coding: utf-8 -*-

"""
MkEWS Job Executor
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

import json
from typing import Optional

from mkews.pymkews_controller.pymkews_controller.data_storage import DataStorage
from mkews.pymkews_controller.pymkews_controller.error import Error


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

class JobValidator:
    VALID_JOB_TYPES = {
        'mkecc-1.0',
        'sleep-1.0'
    }

    @staticmethod
    def validate(user_name: str, data_storage: DataStorage, job_json_path: str = '',
                 job_json: Optional[dict] = None) -> (str, dict):
        if job_json_path:
            with open(job_json_path) as file:
                job_json = json.load(file)

        job_type = job_json['type'] + '-' + job_json['version']

        if job_type not in JobValidator.VALID_JOB_TYPES:
            raise Error(f'Not a valid job type: {job_json["type"]}')

        # Validate dataset_id
        if 'dataset_id' in job_json:
            dts_path = data_storage.get_dataset_url(user_name, job_json['dataset_id'], exists=True)

        return job_type, job_json