# -*- coding: utf-8 -*-

"""
MkEWS Job Executor
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

from typing import Optional
import logging
import json
import os
import subprocess

from ..data_storage import DataStorage
from .job_executor import JobExecutor
from .job_validator import JobValidator


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

class SlurmJobExecutor(JobValidator, JobExecutor):

    def __init__(self, config: dict, data_storage: DataStorage) -> None:
        self.config = config
        self.data_storage = data_storage
        self.logger = logging.getLogger('pymkews.job_executor.slurm')
        self.basedir = os.path.dirname(os.path.realpath(__file__))
        if 'exec' in config:
            self.exec = config['exec']
        else:
            self.exec = os.path.join(self.basedir, 'exec.slurm')

    # -------------------------------------------------------------------------

    @staticmethod
    def __get_job_name(job_id: int) -> str:
        return 'mkews-' + str(job_id)

    # -------------------------------------------------------------------------

    @staticmethod
    def __find_job(job_name: str, stdout: str) -> list:
        lines = str.splitlines(stdout)
        job_info = []

        for line in lines:
            info = dict(entry for entry in (item.split(sep="=") for item in line.split()) if len(entry) == 2)

            if 'JobName' in info and info['JobName'] == job_name:
                job_info.append(info)

        return job_info

    # -------------------------------------------------------------------------

    def validate_job(self, user_name: str, job_json_path: str = '', job_json: Optional[dict] = None) -> (str, dict):
        job_type, job_json = super().validate(user_name, self.data_storage,
                                              job_json_path=job_json_path,
                                              job_json=job_json)

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Validated Job type: {job_type}')

        return job_type, job_json

    # -------------------------------------------------------------------------

    def execute_job(self, user_name: str, job_id: int, job_json: dict) -> None:
        # Create new job in the data storage
        job_url = self.data_storage.create_job(user_name, job_id)

        if 'dataset_id' in job_json:
            dts_url = self.data_storage.get_dataset_url(user_name, job_json['dataset_id'])
        else:
            dts_url = '-'

        self.data_storage.add_data_to_job(user_name, job_id, 'job.json', json.dumps(job_json, indent=2).encode('utf-8'))

        job_name = self.__get_job_name(job_id)
        args = ['sbatch', '-D', job_url, '-t', '00:05:00', '-J', job_name, self.exec, job_url, dts_url, self.basedir]

        self.logger.info(args)
        sp = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Executed process, job_id:{job_id}')

    # -------------------------------------------------------------------------

    def query_job(self, user_name: str, job_id: int) -> (int, dict):
        results = {}

        # Query scontrol
        args = ['scontrol', 'show', 'jobs',  '-o']
        cp = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # Find job
        job_name = self.__get_job_name(job_id)
        jobs = self.__find_job(job_name, cp.stdout.decode('utf-8'))

        # Check job state
        if len(jobs):
            state = jobs[-1]['JobState']

            if self.logger.isEnabledFor(logging.INFO):
                id = jobs[-1]['JobId']
                self.logger.info(f'Process ID {id}, state:{state}')

            if state == 'RUNNING':
                return self.PROCESS_RUNNING, results
            elif state == 'COMPLETING':
                return self.PROCESS_COMPLETING, results
            elif state == 'PENDING':
                return self.PROCESS_PENDING, results

        # Process is not running, that is the exit status?
        jfiles = self.data_storage.list_job_files(user_name, job_id)

        if 'results.json' not in jfiles:
            results['error_message'] = 'Unknown error (No error message returned)'
            return self.PROCESS_FAILURE, results

        try:
            with self.data_storage.get_file_from_job(user_name, job_id, 'results.json') as f:
                results = json.load(f)
                error_message = results['error_message']

                if results['status'] == 'failed':
                    return self.PROCESS_FAILURE, results

                return self.PROCESS_SUCCESS, results
        except Exception:
            results['error_message'] = 'Unknown error (No error message returned)'
            return self.PROCESS_FAILURE, results

    # -------------------------------------------------------------------------

    def delete_job(self, user_name: str, job_id: int) -> None:
        # Call scancel slurm command
        job_name = self.__get_job_name(job_id)
        args = ['scancel', '-n', job_name]
        cp = subprocess.run(args)

        # Delete job
        self.data_storage.delete_job(user_name, job_id)

