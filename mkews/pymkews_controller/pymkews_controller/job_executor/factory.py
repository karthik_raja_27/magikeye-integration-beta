# -*- coding: utf-8 -*-

"""
MkEWS Job Executor
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

from mkews.pymkews_controller.pymkews_controller.error import Error
from ..data_storage import DataStorage
from .local_jobex import LocalJobExecutor
from .testing_jobex import TestingJobExecutor
from .slurm_jobex import SlurmJobExecutor


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

class JobExecutorFactory:

    @staticmethod
    def create(config: dict, data_storage: DataStorage):
        if 'type' not in config:
            raise Error('Unknown JobExecutor type, please specify "type" property in the configuration')

        if config['type'] == 'slurm':
            return SlurmJobExecutor(config['params'], data_storage)
        elif config['type'] == 'local':
            return LocalJobExecutor(config['params'], data_storage)
        elif config['type'] == 'testing':
            return TestingJobExecutor(config['params'], data_storage)
        else:
            raise Error(f'Unknown JobExecutor type: {config["type"]}')
