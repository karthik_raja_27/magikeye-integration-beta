# -*- coding: utf-8 -*-

"""
MkEWS Job Executor
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

from abc import ABC, abstractmethod
from typing import Optional


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

class JobExecutor(ABC):
    PROCESS_UNKNOWN = 0
    PROCESS_RUNNING = 1
    PROCESS_PENDING = 2
    PROCESS_COMPLETING = 2
    PROCESS_SUCCESS = -1
    PROCESS_FAILURE = -2
    PROCESS_CANCELLED = -3

    @abstractmethod
    def validate_job(self, user_name: str, job_json_path: str = '', job_json: Optional[dict] = None) -> (str, dict):
        pass

    @abstractmethod
    def execute_job(self, user_name: str, job_id: int, job_json: dict) -> None:
        pass

    @abstractmethod
    def query_job(self, user_name: str, job_id: int) -> (int, dict):
        pass

    @abstractmethod
    def delete_job(self, user_name: str, job_id: int) -> None:
        pass
