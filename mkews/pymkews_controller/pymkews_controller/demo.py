#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
MkEWS Controller demo
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."


import pymkews_controller
import tempfile
import logging
import os

# ------------------------------------------------------------------------------

if __name__ == "__main__":

    # create logger
    logger = logging.getLogger('pymkews')
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

    # ------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------

    controller_config = {
        'data_storage': {
            'type': 'osfs',
            'params': {
                'root': ''
            }
        },
        'job_executor': {
            'type': 'testing',
            'params': {
                'execution_rate': 0.2,
                'failure_rate': 0.5
            }
        },
    }

    # ------------------------------------------------------------------------------
    # DataStorage

    ds_root = tempfile.TemporaryDirectory()
    controller_config['data_storage']['params']['root'] = ds_root.name
    logger.info(f'Created temporary data storage root: {ds_root.name}')

    dstore = pymkews_controller.DataStorageFactory.create(controller_config['data_storage'])
    jobex = pymkews_controller.JobExecutorFactory.create(controller_config['job_executor'], dstore)

    # ---------------

    dstore.create_user('jheller')
    dstore.create_user('tester')

    users = dstore.list_users()
    logger.info(f'Users in fs: {users}')

    # ---------------

    user_name = 'jheller'
    dstore.create_dataset(user_name, 123)
    dstore.create_dataset(user_name, 312)

    # ---------------

    dts_id = 456

    dstore.create_dataset(user_name, dts_id)

    with tempfile.NamedTemporaryFile() as f:
        f.write(b'Dummy content\n')
        f.flush()
        dstore.add_file_to_dataset(user_name, dts_id, f.name)

    with tempfile.NamedTemporaryFile() as f:
        f.write(b'Dummy content\n')
        f.flush()
        dstore.add_file_to_dataset(user_name, dts_id, f.name)

    dfiles = dstore.list_dataset_files(user_name, dts_id)
    logger.info(f'Files in the dataset "{dts_id}": {dfiles}')

    with dstore.get_file_from_dataset(user_name, dts_id, dfiles[0]) as f:
        data = f.read()
        logger.info(f'File Content: "{dfiles[0]}": {data}')

    try:
        dstore.get_file_from_dataset(user_name, dts_id, 'i_do_not_exist.txt')
    except Exception as err:
        logger.error('Cannot get dataset file: %s' % err)

    dstore.delete_dataset(user_name, dts_id)
    dts = dstore.list_datasets(user_name)
    logger.info(f'Datasets for the user "{user_name}": {dts}')

    # ---------------

    # NOTE: create_job/delete_job call are handled by JobExecutor
    dstore.create_job(user_name, 1)
    dstore.create_job(user_name, 2)
    dstore.create_job(user_name, 3)
    dstore.delete_job(user_name, 1)
    jobs = dstore.list_jobs(user_name)
    logger.info(f'Jobs for the user "{user_name}": {jobs}')

    dstore.delete_user('tester')
    users = dstore.list_users()
    logger.info(f'Users in fs: {users}')

    # ------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------
    # JobExecutor

    user_name = 'jheller'

    invalid_job_json = {
        'type': 'mkecc',
        'version': '1.0',
        'dataset_id': 0,
        'params': {
            'template': 'vcsel_mkedet02_sensor',
            'macros': {
                "LASER_TARGET_GAMMA": 0.4,
                "MAX_DEPTH": 2040
            }
        }
    }

    try:
        job_type, job_json = jobex.validate_job(user_name, job_json=invalid_job_json)
    except Exception as err:
        logger.error('Cannot validate job JSON: %s' % err)

    # Invalid job, interrupt

    job_json = {
        'type': 'mkecc',
        'version': '1.0',
        'dataset_id': 123,
        'params': {
            'template': 'vcsel_mkedet02_sensor',
            'macros': {
                "LASER_TARGET_GAMMA": 0.4,
                "MAX_DEPTH": 2040
            }
        }
    }

    # ------------------------------------------------------------------------------

    job_type, job_json = jobex.validate_job(user_name, job_json=job_json)

    # Job is valid, ask DTB for job_id
    job_id = 123

    # Execute job
    jobex.execute_job(user_name, job_id, job_json)

    pstatus = jobex.PROCESS_UNKNOWN

    while pstatus >= 0:
        pstatus, results = jobex.query_job(user_name, job_id)
        logger.info(f'Job running: {job_id}')

    if pstatus == jobex.PROCESS_SUCCESS:
        logger.info(f'Job successful: {job_id}')
    elif pstatus == jobex.PROCESS_FAILURE:
        logger.info(f'Job failure: {job_id}, {results}')

    logger.info(f'Jobs for the user "{user_name}": {dstore.list_jobs(user_name)}')

    fname = 'job.json'
    with dstore.get_file_from_job(user_name, job_id, fname) as f:
        data = f.read()
        logger.info(f'File Content: "{fname}": {data}')

    try:
        dstore.get_file_from_dataset(user_name, job_id, 'i_do_not_exist.txt')
    except Exception as err:
        logger.error('Cannot get job file: %s' % err)

    jobex.delete_job(user_name, job_id) # This will also delete the job from the data storage
    logger.info(f'Jobs for the user "{user_name}": {dstore.list_jobs(user_name)}')
