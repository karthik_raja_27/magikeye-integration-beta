#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
MkEWS Controller demo2
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."


import pymkews_controller
import tempfile
import logging
import os
import time
import json
import pprint

if __name__ == "__main__":

    # create logger
    logger = logging.getLogger('pymkews')
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

    # ------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------

    exec = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        '..', '..', 'pymkews_worker', 'pymkews_worker', 'pymkews_worker.slurm')

    if not os.path.isfile(exec):
        exec = 'pymkews_worker'

    controller_config = {
        'data_storage': {
            'type': 'osfs',
            'params': {
                'root': ''
            }
        },
        'job_executor': {
            'type': 'slurm',
            'params': {
            }
        }
    }

    # ------------------------------------------------------------------------------
    # DataStorage

    #ds_root = tempfile.TemporaryDirectory()
    #controller_config['data_storage']['params']['root'] = ds_root.name
    #logger.info(f'Created temporary data storage root: {ds_root.name}')

    ds_root = '/data/data_storage' #tempfile.TemporaryDirectory()
    controller_config['data_storage']['params']['root'] = ds_root
    logger.info(f'Created temporary data storage root: {ds_root}')

    dstore = pymkews_controller.DataStorageFactory.create(controller_config['data_storage'])
    jobex = pymkews_controller.JobExecutorFactory.create(controller_config['job_executor'], dstore)

    try:
        dstore.create_user('jheller')
    except:
        pass

    # ------------------------------------------------------------------------------

    user_name = 'jheller'
    dts_id = 1
    fname = '/data/py_out.json'

    try:
        dstore.create_dataset(user_name, dts_id)
    except:
        pass

    dstore.add_file_to_dataset(user_name, dts_id, fname)

    # ------------------------------------------------------------------------------

    job_id = 2
    job_json = {
         'type': 'mkecc',
         'version': '1.0',
         'dataset_id': dts_id,
         'params': {
             'dsession': os.path.basename(fname),
             'template': 'vcsel_mkedet02_sensor',
             'macros': {}
         }
    }

    #job_json = {
    #    'type': 'sleep',
    #    'version': '1.0',
    #    'params': {
    #        'duration': 5
    #    }
    #}

    job_type, job_json = jobex.validate_job(user_name, job_json=job_json)
    jobex.execute_job(user_name, job_id, job_json)

    pstatus = jobex.PROCESS_UNKNOWN

    while pstatus >= 0:
        pstatus, results = jobex.query_job(user_name, job_id)
        logger.info(f'Job running: {job_id}')
        time.sleep(0.5)

    if pstatus == jobex.PROCESS_SUCCESS:
            res = json.dumps(results, indent=2)
            logger.info(f'File Content: "{fname}": \n{res}')
    elif pstatus == jobex.PROCESS_FAILURE:
        pprint.pprint(results)
        logger.info(f'Job failure: {job_id}, {results["error_message"]}')

